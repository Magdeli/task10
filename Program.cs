﻿using System;
using System.Collections.Generic;


namespace Task10
{
    class Program
    {
        static void Main(string[] args)
        {
            // This program has a list of names and it lets the user 
            // search after names and list all names.

            #region Variables
            // In this region all variables are declared

            string input;
            string searchInput;
            //string[] namesString = { "Lars Asplin", "Magdeli Asplin", "Reidun Holmøy", "Jostein Utkilen", "Pernille Asplin" };
            List<Person> personList = new List<Person>();

            personList.Add(new Person("Magdeli", "Asplin", 41037696, 25));
            personList.Add(new Person("Lars", "Asplin"));
            personList.Add(new Person("Reidun", "Holmøy"));
            personList.Add(new Person("Jostein", "Utkilen"));
            personList.Add(new Person("Pernille", "Asplin"));

            #endregion

            #region Input search for name or list all names
            // In this region the input on whether the user want to search for a name or 
            // list all names is gathered.

            do
            {
                Console.WriteLine();
                Console.WriteLine("Hello, this program has a list of names. If you want to search, type 'search', and if you want to list all the names, type 'list'. If you want to end the program, type 'end':");
                Console.WriteLine();

                input = Console.ReadLine();

                #region Case "search"
                // This region handles the case "search" by using the methods getFullname and
                // printName from the class Person.

                if (input == "search")
                {
                    Console.WriteLine("Type your searchword:");
                    searchInput = Console.ReadLine().ToLower();


                    foreach (Person person in personList)
                    {
                        if (person.getFullname().Contains(searchInput))
                        {
                            Console.WriteLine(person.printName());
                        }
                    }
                }

                #endregion

                #region Case "list"
                //This region handles the case "list", using the method printName from the class
                // Person.

                if (input == "list")
                {
                    foreach(Person person in personList)
                    {
                        Console.WriteLine(person.printName());
                    }
                }
            } while (input != "end"); //The code willl let you list and search infinitly until you type "end" to end the program

            #endregion

            #endregion
        }
    }
}

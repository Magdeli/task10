﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10
{
    class Person
    {
        #region Attributes
       // Here all the variables are declared.

        private string firstName;
        private string surName;
        private int phoneNumber;
        private int age;

        #endregion

        #region Constructors
        // In this region the constructors are made. There are currently three different types;
        // a empty one, one that takes the arguments of first name and surname, and one that takes
        // the argument first name, last name, phone number and age.

        public Person() { }

        public Person(string firstName, string surName)
        {
            this.firstName = firstName;
            this.surName = surName;
        }

        public Person(string firstName, string surName, int phoneNumber, int age)
        {
            this.firstName = firstName;
            this.surName = surName;
            this.phoneNumber = phoneNumber;
            this.age = age;
        }

        #endregion

        #region Behaviour
        // There are two methods in this class; one that returns a string containing the full name
        // in lower case letters, and one that returns a string containing the full name as it is.

        public string getFullname()
        {
            return (firstName + " " + surName).ToLower();
        }

        public string printName()
        {
            return (firstName + " " + surName);
        }

        #endregion
    }
}
